def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == '+':
        ans = a + b
    elif operator == '-':
        ans = a - b
    elif operator == '*':
        ans = a * b
    else:
        ans = a / b

    ans = int(ans)
    new_ans = ""
    while ans > 0:
        new_ans = str(int(ans % 2)) + new_ans
        ans = (ans - (ans % 2)) / 2

    return int(new_ans)

        
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
