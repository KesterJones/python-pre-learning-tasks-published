def vowel_swapper(string):
    # ==============
    # Your code here
    #string = string.replace('O', '000', 1)
    first = {'a': True, 'e': True, 'i': True, 'o': True, 'u': True}
    new_string = ""

    for i in range(0, len(string)):
        if string[i].lower() in 'aeiou':
            if first[string[i].lower()] == True:
                first[string[i].lower()] = False
            elif first[string[i].lower()] == False:
                first[string[i].lower()] = "done"
                if string[i].lower() == 'a':
                    string = string[:i] + string[i:].replace(string[i], '4', 1)
                if string[i].lower() == 'e':
                    string = string[:i] + string[i:].replace(string[i], '3', 1)
                if string[i].lower() == 'i':
                    string = string[:i] + string[i:].replace(string[i], '!', 1)
                if string[i].lower() == 'u':
                    string = string[:i] + string[i:].replace(string[i], '|_|', 1)
                if string[i].lower() == 'o':
                    if string[i] == 'o':
                        string = string[:i] + string[i:].replace(string[i], 'ooo', 1)
                    else:
                        string = string[:i] + string[i:].replace(string[i], '000', 1)      
  
    return string
   
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
